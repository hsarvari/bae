import numpy as np
from numpy import linalg as la
import torch

__author__ = 'Hamed Sarvari, Bardh Prenkaj, Giovanni Stilo'

class NormDiffStopper:
    def __init__(self, precision, norm_kind=np.inf):
        self.norm_kind = norm_kind
        self.precision = precision

    def stop(self, prev, curr, it):
        dist = torch.abs(prev - curr)
        return it >= 1 and la.norm(dist, self.norm_kind)/len(dist) <= self.precision

class FixedStopper:
    def __init__(self, maxit):
        self.maxit = maxit


    def stop(self, prev, curr, it):
        return it  >= self.maxit


class LossValueStopper:

    def __init__(self, threshold):
        self.threshold = threshold

    def stop(self, prev, curr, it):
        return it > 1 and abs(prev - curr) < self.threshold