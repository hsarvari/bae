import numpy as np
import powerlaw
from collections import Counter
from scipy.stats import entropy

__author__ = 'Hamed Sarvari, Bardh Prenkaj, Giovanni Stilo'

def combine_sum(matrix):
    return np.sum(matrix, axis=0)

def combine_last(matrix):
    return np.sum(matrix[-1], axis=0)

def combine_best(matrix):
    # get the row-wise sum of rec errors
    s = np.sum(matrix, axis=1)

    # get the first index of the component with the 
    # lowest rec error
    min_rec_i = np.where(s == np.amin(s))[0][0]
    # return all the rec errors of the data points
    # in the min_rec_i-th row of the original matrix
    return matrix[min_rec_i]

def combine_fromsecond(matrix):
    return np.sum(matrix[1:], axis=0)

def combine_weighted(matrix):
    # do not consider the first iteration of BAE
    num = np.sum(matrix[1:], axis=1)
    weights = (1 / num) / np.sum(1 / num)

    return np.sum(matrix[1:] * weights[:, np.newaxis],\
        axis=0)

def combine_kl_divergence(matrix):
    # discard the first component from the ensemble
    matrix = matrix[1:]

    weights = list()
    # loop through all the reconstruction errors
    for rec_errors in matrix:
        # fit a power law of the rec errors
        res = powerlaw.Fit(rec_errors,\
        xmin=rec_errors.min(), xmax=rec_errors.max(),\
            discrete=True)
        # get the occurrence probability of each
        # data point
        _, prob = res.ccdf()
        # generate our own distribution of data
        y = np.array(list(Counter(rec_errors).values()))
        # scale it to [0,1]
        y = y/y.max()
        # calculate the kl-divergence between
        # our distribution and the power law
        score = entropy(y, prob)
        # append the kl-divergence to the
        # list of weights
        weights.append(score)

    weights = np.array(weights)

    return np.sum(matrix * weights[:, np.newaxis], axis=0)




