import gzip
import json
import os
import uuid

import numpy as np
import pandas as pd
import torch

__author__ = 'Hamed Sarvari, Bardh Prenkaj, Giovanni Stilo'

NS_AN_SCORES = "scores"
NS_REC_ERROR_IT = "rec-error-it"
NS_RANKING =  "ranking"
NS_WEIGHTS =  "weights"
NS_LOSS = "loss-value"
SEP =','

class MetaInfo:

    def __init__(self, params):

        self.expid = uuid.uuid1().hex
        self.params = params          
        self.params['eid']=self.expid

class ExperimentDumper:
    
    def __init__(self, meta_info, path="../resources/traces"):

        self.path=path
        self.meta_info=meta_info
        
        self.namespaces = dict()

        dirname = os.path.join(self.path,\
            self.meta_info.params['group'],\
                self.meta_info.expid)

        self.meta_info.params['trace-path'] = dirname

        os.makedirs(dirname)
        
        with open(os.path.join(self.path,\
            self.meta_info.expid \
                + '.json'), 'w') as outfile:  
            json.dump(self.meta_info.params, outfile)

    def trace_array(self,namespace,data):

        self.get_writer(namespace)\
            .write(SEP.join([str(i) for i in data])\
                + '\n')

    def trace_model(self, model, it):
        
        torch.save(model, os.path.join(self.path,\
             self.meta_info.params['group'],\
                 self.meta_info.expid,\
                     model.__class__.__name__\
                         + "@" + it + ".pth"))

    def close(self):
        for k in self.namespaces.keys():
            self.namespaces[k].close()
        
    def get_writer(self, namespace):

        if self.namespaces.get(namespace,None) is None:

                self.namespaces[namespace] = gzip.open(\
                    os.path.join(self.path,\
                    self.meta_info.params['group'],\
                        self.meta_info.expid, namespace\
                            + ".gz"), "wt")
                self.namespaces[namespace].write('# '\
                + namespace + ' for :'\
                    + json.dumps(self.meta_info.params)\
                        + '\n')
        
        return self.namespaces[namespace]
        

class LoadDump:

    def __init__(self, path='../resources/traces'):

        self.path = path
        
        self.descriptors = [f for f in \
            os.listdir(self.path) \
                if os.path.isfile(os.path\
                    .join(self.path, f))]


    def filter(self, query):
        results = []
        for descriptor in self.descriptors:
            data = json.load(open(os\
                .path.join(self.path,\
                    descriptor), 'r'))

            if all(item in data.items()\
                for item in query.items()):

                results.append(\
                    data.get('trace-path',None))

        return results

    def load(self, trace, doc=NS_REC_ERROR_IT):

        try:
            with gzip.open(os.path.join(trace,\
                doc + '.gz'), "r") as f:

                res = pd.read_csv(f,\
                    skiprows=1, header=None).values

                res = np.array(res, dtype=np.double)

            return res
        except:
            return None
