from __future__ import print_function

import collections

import numpy as np
import torch

import utils.dump as dumper
from config import conf_sample as conf
from models.fcae import FullyConnectedAE
from sampling.samplers import WeightedSampler
from utils.data import DataLoader
from stoppage.stoppers import LossValueStopper

__author__ = 'bardhp95'


def get_model(enc_layers, dim):

    model = FullyConnectedAE(enc_layers, dim,\
        conf.shrinker, bottleneck=conf.BOTTLENECK)

    opt = torch.optim.Adam(model.parameters(),\
        lr=conf.LR,\
        amsgrad=True, weight_decay=conf.WEIGHT_DECAY)

    return model, opt

"""
    Function that resets the reconstruction
    errors to 0 after each fold is done
    executing

    Parameters:
        - dim (int): represents the
                dimensionality of the data
                points

    returns numpy array of zeros
"""
def reset_errors(dim):
    return np.array([0 for i in range(dim)])

"""
    Function which gets the reconstruction errors
    for each data point

    Parameters:
        - model (?): the trained torch model
        - X (torch.tensor): the original data points
    
    returns numpy.array with the reconstruction errors
        of each data point
"""
def get_error(model, X):
    eval_err = torch.nn.MSELoss()
    return np.array([eval_err(model(X[i]),\
        X[i]).data.item() for i in range(X.shape[0])])

def shift_losses(losses, next_loss):
    losses[0] = losses[-1]
    losses[-1] = next_loss
    return losses

# define the training stopper
stopper = LossValueStopper(conf.STOP_TH)
# iterate the list of datasets
for dataset in conf.DATASETS:
    ############### Dataset loader ######################
    loader = DataLoader(conf.DPATH + dataset,\
        normalise=conf.NORM, b=conf.B_NORM,\
            f=conf.E_NORM)

    X, y = loader.prepare_data()

    sampler = WeightedSampler(X)

    for enc_layer in range(1,conf.MAX_ENC_LAYERS):
        # what's the number of layers when taking in input
        # the number of encoder layers? It depends if
        # we're considering a single bottleneck or not
        num_layers = enc_layer*2+1 if conf.BOTTLENECK else enc_layer*2
        print(f'Training model with {num_layers} layers...')
        # get the model with the specified number
        # of encoder layers and the data
        # dimensionality
        model, opt = get_model(enc_layer, X.shape[1])
        # perform experiments for the
        # specified number of folds
        for fold in range(conf.FOLDS):
            rankings = list()
            # reset the training data indices
            rec_errors = reset_errors(X.shape[0])

            print(f'Dataset {dataset} fold {fold+1}')
            # Dump and loader characteristics
            params = dict()
            params['optimiser'] = opt.__class__.__name__
            params['group'] = model.__class__.__name__
            params['dataset'] = dataset
            params['run'] = fold + 1
            params['loss'] = conf.loss.__class__.__name__
            params['normaliser'] = None
            params['stopper'] = None
            params['layer_num'] = num_layers

            logger = dumper.ExperimentDumper(dumper\
                .MetaInfo(params))

            # repeat the training for a certain amount 
            # of components
            for it in range(conf.ITERATIONS):
                # initialize the previous and current 
                # training losses
                # losses[0] -> previous loss
                # losses[1] -> current loss
                losses = [-1,-1]
                # reset the model's weights for the
                # next AE component
                model.apply(model.weight_reset)
                # sample the virtual dataset 
                # \mathbf{X}^{(i)}
                # as described in the paper 
                X_i = sampler.sample(rec_errors, it)
                # tell torch that the virtual dataset 
                # doesn't need to compute the gradients
                X_i.requires_grad_(False)
                
                print(f'Training component {it+1}')
                # train the components for a certain
                # number of epochs
                for epoch in range(conf.EPOCHS):
                    model.train()
                    # get the reonstructed output
                    # of the virtual dataset 
                    # \mathbf{X}^{(i)}
                    X_i_out = model(X_i)
                    # get the loss according to the 
                    # MSE criterion
                    loss = conf.loss(X_i_out, X_i)

                    losses = shift_losses(losses, loss)
                    # zero the old gradients for the 
                    # backpropagation phase
                    opt.zero_grad()
                    # compute the backpropagation graph
                    loss.backward()
                    # update the weights of the ADAM 
                    # optimiser
                    opt.step()

                    if (epoch + 1) % 50 == 0:
                        print(f'Epoch {epoch+1} - loss: {loss.item()}')

                    if stopper.stop(losses[0], losses[1], epoch):
                        print(f'Loss difference {abs(losses[0]-losses[1])} < {conf.STOP_TH}')
                        print(f'Breaking the training of iteration {it+1} at epoch {epoch+1}')
                        break
                # evaluate the reconstruction errors of 
                # the trained
                # model in the original datasetd
                rec_errors = get_error(model, X)

                print(f'Finished training component {it+1}')
                print(f'Component {it+1}: loss {loss.item()} sum of rec_errors {np.sum(rec_errors)}')
                # append the reconstruction errors in 
                # the ranking list
                # this is used to compute the consensus 
                # function
                rankings.append(rec_errors)
                # trace reconstruction errors for each
                # iteration
                logger.trace_array(dumper\
                    .NS_REC_ERROR_IT, rec_errors)
                # trace the loss of each iteration
                logger.trace_array(dumper.NS_LOSS,\
                    [loss.data.numpy()])
                # trace the model for each iteration
                logger.trace_model(model, str(it + 1))

            # combine the overall ranking
            final_ranking = conf.consensus(rankings)
            # log the overall ranking
            logger.trace_array(dumper.NS_RANKING,\
                final_ranking)

    print(f'Finished training BAE with a base architecture of {num_layers} layers.')
